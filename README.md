Rails Version: 5.1.4      
Ruby Version: 2.4.1     
To test: GET api/v1/questions?lower=10;upper=20   
Output (JSON):   
question: "13 + 3"   
answer: 16    
options:[12, 14, 18]    
Returns an error if invalid arguments are given.    