module Api
  module V1
    class QuestionsController < ApplicationController
      ''' renders JSON if valid arguments are given. Sort of a main '''
      def question
        check_args = check_arguments(params[:lower], params[:upper])
        if check_args.is_a?(String)

          render html: "Error: #{check_args}"
        else
          lower, upper = check_args
          render json: create_question(lower, upper)
        end
      end
      '''Handles all the errors. Return: err '''
      def check_arguments(lower, upper)
        if lower != nil and upper != nil
          lower = lower.to_i
          upper = upper.to_i
          if lower >= upper
            return "Lower bound has to smaller than upper bound."
          end

          if lower <= 0 or upper > 1000000
            return "Range has to between 0 and 1,000,000"
          end
          return lower, upper
        else
          error = ''
          if lower == nil
            error += "Missing Argument: Lower Bound\n"
          end
          if upper == nil
            error += "Missing Argument: Upper Bound"
          end
          return error
        end
      end

      ''' Creates the question and returns JSON'''
      def create_question(lower, upper)
        answer = upper+lower
        if answer > upper or answer < lower
          while answer > upper or answer < lower
            #num1 = rand(1-lower)
            num1 = rand(lower)
            num2 = rand(upper)
            answer = num1 + num2
          end
        end

        answer = num1 + num2
        options = gen_options(answer)
        question = "#{num1} + #{num2}?"
        json = { question: question,
                  answer: answer,
                options: options}
        return json

      end
''' Generates three wrong options. Return: Options'''
      def gen_options(answer)
        options = []
        while options.length != 3
          temp_var_lower = answer-5
          temp_var_upper = answer+5
          temp = rand(temp_var_lower..temp_var_upper)
          if answer != temp and !options.include?(temp)
            options.push(temp)
          end
        end
        return options
      end
    end
  end
end
